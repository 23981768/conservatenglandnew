//Scene and Camera settings
var scene = new THREE.Scene();
scene.background = new THREE.Color(0xffffff);
			
var camera = new THREE.PerspectiveCamera(
    75, 
    window.innerWidth / window.innerHeight, 
    0.1, 
    1000);

camera.position.z = 25;
camera.position.y = 5;

//Renderer settings					
var renderer = new THREE.WebGLRenderer({antialias : true});
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor("#e5e5e5");
document.body.appendChild(renderer.domElement);

window.addEventListener('resize', () => {
	renderer.setSize(window.innerWidth,window.innerHeight);
	camera.aspect = window.innerWidth / window.innerHeight;

	camera.updateProjectionMatrix();
});

//Raycaster and mouse 

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

function onMouseMove( event ) {

	mouse.x = (event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = ( event.clientY / window.innerHeight) * 2 + 1;
}

//Set a colour and a geometry for the shapes
var region_geometry = new THREE.BoxGeometry(3, 3, 3);
var green_color = new THREE.Color(0x02660c);
var selected_color = new THREE.Color(0x06b817);
var clicked_color = new THREE.Color(0xFFFFFF);

const extrudeSettings = {
	steps: 2,
	depth: 1.5,
	bevelEnabled: true,
	bevelThickness: 0.2,
	bevelSize: 0.35,
	bevelOffset: 0,
	bevelSegments: 1
};

////////////////////////
///GEOMETRY VARIABLES///
////////////////////////

//Uninteractable geometry

//North East
const NEshape = new THREE.Shape();
NEshape.moveTo(3, 2.2); //Beginning
NEshape.lineTo(1.2, 2.2); //1
NEshape.lineTo(-1, 3); //2
NEshape.lineTo(-1, 5); //3
NEshape.lineTo(-1.8, 7);//4
NEshape.lineTo(-1.3, 9); //5
NEshape.lineTo(-2, 11.5); //6
NEshape.lineTo(1, 13);//7
NEshape.lineTo(1, 10);//8
NEshape.lineTo(1, 8);//8
NEshape.lineTo(3, 6);//8
NEshape.lineTo(3, 4);
NEshape.lineTo(3.5, 3.6);
NEshape.lineTo(3, 2.2);

const NEgeometry = new THREE.ExtrudeGeometry( NEshape, extrudeSettings );
const NEmaterial = new THREE.MeshPhongMaterial( { color: green_color } );
const NEmesh = new THREE.Mesh( NEgeometry, NEmaterial );
NEmesh.name = 'Northeast';
scene.add( NEmesh );

//North West
const NWshape = new THREE.Shape();
NWshape.moveTo(-4, 1.5);//Beginning
NWshape.lineTo(-2.5, 1.5); //1
NWshape.lineTo(-2, 2.8);//2
NWshape.lineTo(-2, 5);//3
NWshape.lineTo(-2.8, 7);//4
NWshape.lineTo(-2.5, 9); //5
NWshape.lineTo(-3, 11); //6
NWshape.lineTo(-5.5, 10);//7
NWshape.lineTo(-6, 9); //8
NWshape.lineTo(-5.8, 7); //9
NWshape.lineTo(-4.8, 6);//10
NWshape.lineTo(-4.8, 5); //11
NWshape.lineTo(-5.8, 4); //12
NWshape.lineTo(-5, 2);
NWshape.lineTo(-4, 1.5);

const NWgeometry = new THREE.ExtrudeGeometry( NWshape, extrudeSettings );
const NWmaterial = new THREE.MeshPhongMaterial( { color: green_color } );
const NWmesh = new THREE.Mesh( NWgeometry, NWmaterial );
NWmesh.name = 'Northwest';
scene.add( NWmesh );

//Midlands
const MLshape = new THREE.Shape();
MLshape.moveTo(-4, -4); //beginning
MLshape.lineTo(0, -4); //1
MLshape.lineTo(1.2, -2.6) //2
MLshape.lineTo(4, -1.5); //3
MLshape.lineTo(4, 1); //4
MLshape.lineTo(1.2, 1); //5
MLshape.lineTo(-1.5, 2); //6
MLshape.lineTo(-2, 0.5) //7
MLshape.lineTo(-4, 0.5); //8
MLshape.lineTo(-5, 0); //9
MLshape.lineTo(-5, -3); //10
MLshape.lineTo(-4, -4); //11

const MLgeometry = new THREE.ExtrudeGeometry( MLshape, extrudeSettings );
const MLmaterial = new THREE.MeshPhongMaterial( { color: green_color } );
const MLmesh = new THREE.Mesh( MLgeometry, MLmaterial );
MLmesh.name = 'Midlands';
scene.add( MLmesh );

//Southwest
const SWshape = new THREE.Shape();
SWshape.moveTo(-2.5, -5.2); //beginning
SWshape.lineTo(-2.5, -10); //1
SWshape.lineTo(-5, -10);//2
SWshape.lineTo(-5.4, -9.2);//3
SWshape.lineTo(-7, -9.2);//4
SWshape.lineTo(-8, -11);//5
SWshape.lineTo(-8.5, -11);//6
SWshape.lineTo(-11, -10.5);//7
SWshape.lineTo(-12, -10.5);//8
SWshape.lineTo(-12, -10);//9
SWshape.lineTo(-8.8, -8);//10
SWshape.lineTo(-8, -8.25);//11
SWshape.lineTo(-7.5, -7.8)//12
SWshape.lineTo(-7.25, -7);//13
SWshape.lineTo(-6, -7);
SWshape.lineTo(-5, -5.2);
SWshape.lineTo(-2.5, -5.2);

const SWgeometry = new THREE.ExtrudeGeometry( SWshape, extrudeSettings );
const SWmaterial = new THREE.MeshPhongMaterial( { color: green_color } );
const SWmesh = new THREE.Mesh( SWgeometry, SWmaterial );
SWmesh.name = 'Southwest';
scene.add( SWmesh );


//Southeast
const SEshape = new THREE.Shape();
SEshape.moveTo( -1.5, -5.2 );//beginning
SEshape.lineTo( -1.5, -10 ); //1
SEshape.lineTo( -0.75, -11 ); //2
SEshape.lineTo( 1.25, -11.5 ); //3
SEshape.lineTo( 6, -11); //4
SEshape.lineTo( 6, -10.5); //5
SEshape.lineTo( 5.2, -10.5); //6
SEshape.lineTo( 4, -9.5) //7
SEshape.lineTo( 5.2, -7) //8
SEshape.lineTo( 5.5, -5.2) //9
SEshape.lineTo( 4, -3.8) //10
SEshape.lineTo( 2, -3.4) //11
SEshape.lineTo( 0.5, -5.2 )//12
SEshape.lineTo( -1.5, -5.2 );//end

const SEgeometry = new THREE.ExtrudeGeometry( SEshape, extrudeSettings );
const SEmaterial = new THREE.MeshPhongMaterial( { color: green_color } );
const SEmesh = new THREE.Mesh( SEgeometry, SEmaterial );
SEmesh.name = 'Southeast';
scene.add( SEmesh );

//Light settings
var light = new THREE.PointLight(0xFFFFFF, 1, 500);
light.position.set(0, 0, 25);
scene.add(light);
			
var mmi = new MouseMeshInteraction(scene, camera);

//////////////////////////////////
//NORTH WEST REGION INTERACTIONS//
//////////////////////////////////
var showing = false;
var node = null;

mmi.addHandler('Northwest', 'mouseenter', function(mesh) {
	if(showing != true){
		gsap.to(mesh.scale, {duration: 0.5, z: 1.5});
		mesh.material.color = selected_color;
		gsap.to('#NWText', {duration: 1, display: 'block', opacity: 1, x: 10, z: 5, delay: 0.2});
		gsap.to('#NorthwestInformation', {duration: 1, display: 'grid', opacity: .8, x: 10, z: 5, delay: 0.2});
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 0, x: -20, z: -10, display:'none'});
	}
});

mmi.addHandler('Northwest', 'mouseleave', function(mesh) {	
	if( showing != true ){
		gsap.to(mesh.scale, {duration: 0.5, z:1});
		mesh.material.color = green_color;
		gsap.to('#NWText', {duration:1, display:'none', opacity:0, x:-10, z: -5, delay:0.2});
		gsap.to('#NorthwestInformation', {duration: 1, opacity: 0, x: -10, display: 'none', delay: 0.2});
		gsap.to('#GeneralInformation', {delay: 1, duration: 0.2, opacity: 1, x: 20, z: 10, display:'block'});
	} else if ( node == 1 ){
		gsap.to('#NorthwestInformation', {duration: -1, opacity: 1}); 

	}
});

mmi.addHandler('Northwest', 'click', function(mesh) {
	mesh.material.color = selected_color;
	gsap.to('#NWText', {duration: -1,  display: 'grid', opacity: 1});
	gsap.to('#NorthwestInformation', {duration: -1, display: 'grid', opacity: 1});
	showing = true;
	node = 1;
	
});

//////////////////////////////////
//NORTH EAST REGION INTERACTIONS//
//////////////////////////////////

mmi.addHandler('Northeast', 'mouseenter', function(mesh) {
	if( showing != true) {
		gsap.to(mesh.scale, {duration: 0.5, z: 1.5});
		mesh.material.color = selected_color;
		gsap.to('#NEText', {duration:1, display:'block', opacity:1, x:-10, z: 5, delay:0.2});
		gsap.to('#NortheastInformation', {duration: 1, display: 'grid', opacity: .8, x: -10, z: 5, delay: 0.2});
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 0, x: -20, z: -10, display:'none'});
	}
	
});

mmi.addHandler('Northeast', 'mouseleave', function(mesh) {	
if( showing != true){
		gsap.to(mesh.scale, {duration: 0.5, z:1});
		mesh.material.color = green_color;
		gsap.to('#NEText', {duration:1, display:'none', opacity:0, x:10, z: -5, delay:0.2});
		gsap.to('#NortheastInformation', {duration: 1, opacity: 0, x: 10, z:-5, display: 'none', delay: 0.2})
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block'});
	} else if ( node == 2 ){
		gsap.to('#NortheastInformation', {duration: -1, opacity: 1});
		
	}


});

mmi.addHandler('Northeast', 'click', function(mesh) {
	mesh.material.color = selected_color;
	gsap.to('#NEText', {duration: -1,  display: 'grid', opacity: 1});
	gsap.to('#NortheastInformation', {duration: -1, display: 'grid', opacity: 1});
	showing = true;
	node = 2;
	
});

////////////////////////////////
//MIDLANDS REGION INTERACTIONS//
////////////////////////////////

mmi.addHandler('Midlands', 'mouseenter', function(mesh) {
	if( showing != true) {
		gsap.to(mesh.scale, {duration: 0.5, z: 1.5});
		mesh.material.color = selected_color;
		gsap.to('#MLText', {duration:1, display:'block', opacity:1, x:-10, z: 5, delay:0.2});
		gsap.to('#MidlandsInformation', {duration: 1, display: 'grid', opacity: 1, x: -10, z: 5, delay: 0.2});
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 0, x: -20, z: -10, display:'none'});
	}	
});

mmi.addHandler('Midlands', 'mouseleave', function(mesh) {	
if( showing != true){
		gsap.to(mesh.scale, {duration: 0.5, z:1});
		mesh.material.color = green_color;
		gsap.to('#MLText', {duration:1, display:'none', opacity:0, x: 10, z: 5, delay:0.2});
		gsap.to('#MidlandsInformation', {duration: 1, opacity: 0, x: 10, z:5, display:'none', delay: 0.2})
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block'});
	} else if ( node == 4 ){
		gsap.to('#MidlandsInformation', {duration: -1, opacity: 1});
	}
	
});

mmi.addHandler('Midlands', 'click', function(mesh) {
	mesh.material.color = selected_color;
	gsap.to('#MLText', {duration: -1,  display: 'grid', opacity: 1});
	gsap.to('#MidlandsInformation', {duration: -1, display:'grid', opacity: 1});
	showing = true;
	node == 4;
	
});

//////////////////////////////////
//SOUTH EAST REGION INTERACTIONS//
//////////////////////////////////

mmi.addHandler('Southeast', 'mouseenter', function(mesh) {
	if( showing != true) {
		gsap.to(mesh.scale, {duration: 0.5, z: 1.5});
		mesh.material.color = selected_color;
		gsap.to('#SEText', {duration:0.5, display:'block', opacity:1, x:10, z: 5, delay:0.2});
		gsap.to('#SoutheastInformation', {duration: 0.5, display: 'grid', opacity: 1, x: 10, z: 5, delay: 0.2});
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 0, x: -20, z: -10, });
	}	
});

mmi.addHandler('Southeast', 'mouseleave', function(mesh) {
	if( showing != true){
		gsap.to(mesh.scale, {duration: 0.5, z:1});
		mesh.material.color = green_color;
		gsap.to('#SoutheastInformation', {duration: 1, opacity: 0, x: -10, z: -5, display:'none', delay: 0.2})
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block', delay:0.5});
		gsap.to('#SEText', {duration:1, display:'none', opacity:0, x:-10, z: -5, delay:0.2});
	} else if ( node == 4 ){
		gsap.to('#SoutheastInformation', {duration: -1, display: 'grid', opacity: 1});
	}
	
});

mmi.addHandler('Southeast', 'click', function(mesh) {
	mesh.material.color = selected_color;
	gsap.to('#SEText', {duration: -1,  display: 'grid', opacity: 1});
	gsap.to('#SoutheastInformation', {duration: -1,  display: 'grid', opacity: 1});
	showing = true;
	node == 4;
	
});

//////////////////////////////////
//SOUTH WEST REGION INTERACTIONS//
//////////////////////////////////

mmi.addHandler('Southwest', 'mouseenter', function(mesh) {
	if( showing != true) {
		gsap.to(mesh.scale, {duration: 0.5, z: 1.5});
		mesh.material.color = selected_color;
		gsap.to('#SWText', {duration:0.5, display:'block', opacity:1, x:10, z: 5, delay:0.2});
		gsap.to('#SouthwestInformation', {duration: 1, display:'grid', opacity: 1, x: 10, z: 5, delay: 0.2});
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 0, x: -20, z: -10});
	}	
	
});

mmi.addHandler('Southwest', 'mouseleave', function(mesh) {
	if( showing != true){
		gsap.to(mesh.scale, {duration: 0.5, z:1});
		mesh.material.color = green_color;
		gsap.to('#SouthwestInformation', {duration: 1, opacity: 0, x: -10, z:-5, display:'none', delay:0.2})
		gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block', delay:0.5});
		gsap.to('#SWText', {duration:1, display:'none', opacity:0, x:-10, z:-5, delay:0.2});
	} else if ( node == 5 ){
		gsap.to('#SouthwestInformation', {duration: -1, display:'grid', opacity: 1});
	}
	
});

mmi.addHandler('Southwest', 'click', function(mesh) {
	mesh.material.color = selected_color;
	gsap.to('#SWText', {duration: -1,  display: 'grid', opacity: 1});
	gsap.to('#MSouthwestInformation', {duration: -1, display:'grid', opacity: 1});
	showing = true;
	node == 5;
	
});

//Span close functions
window.onload = function(){

	
	var closebtns = document.getElementsByClassName('close');
	var i;

	for(i=0; i < closebtns.length; i++) {
		closebtns[i].addEventListener('click', function() {

			//NW Close
			gsap.to('#NWText', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#NorthwestInformation', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block', delay:0.5});
			gsap.to(NWmesh.scale, {duration: 0.5, z:1});
			NWmesh.material.color = green_color;
			
			//NE Close
			gsap.to('#NEText', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#NortheastInformation', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block', delay:0.5});
			gsap.to(NEmesh.scale, {duration: 0.5, z:1});
			NEmesh.material.color = green_color;

			//ML Close
			gsap.to('#MLText', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#MidlandsInformation', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block', delay:0.5});
			gsap.to(MLmesh.scale, {duration: 0.5, z:1});
			MLmesh.material.color = green_color;

			//SW Close
			gsap.to('#SWText', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#SouthwestInformation', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block', delay:0.5});
			gsap.to(SWmesh.scale, {duration: 0.5, z:1});
			SWmesh.material.color = green_color;

			//SE Close
			gsap.to('#SEText', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#SoutheastInformation', {duration: 1, opacity: 0, x: -10, delay: 0.2})
			gsap.to('#GeneralInformation', {duration: 0.2, opacity: 1, x: -20, z: 10, display:'block', delay:0.5});
			gsap.to(SEmesh.scale, {duration: 0.5, z:1});
			SEmesh.material.color = green_color;

			showing = false;
		});
	}
}

var render = function() {
	raycaster.setFromCamera( mouse, camera);
	var intersects = raycaster.intersectObjects( scene.children );
	requestAnimationFrame(render);
	
	mmi.update();
	
	renderer.render(scene, camera);
}

render();